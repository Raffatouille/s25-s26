// Creating a basic server setup

// Node JS => will provide us a runtime environment or RTE that will allow us to execute our program/application

// RTE (Runtime Enviroment) - this pertains to an evironment or system in which a program can be executed.



// CREATE A STANDARD SERVER SETUP USING 'PLAIN' NODE.JS

// 1 . identify and prepare the components/ingredients that you would need in order to execute the task
    // http - hypertext transfer protocol - main ingredient when creating a server using plain node.js.
    // -is a built in module of NODE.JS, that will allow us to 'establish a connection' and will make it possible to transfer data over HTTP.

    // you need to get the component first. 

    // use the require() directive.
        // this will allow to gather and aquire certain packages that we will use to build our application.

const http = require('http');
    // http will provide us the components needed to establish a server.

    // http contains the utility constructor called "createServer", which will allow us to create an HTTP server.


// 2. Identify and describe a location where the connection will happen. In order to provide a proper medium for both parties (client and server), we will then find the connection to the desired port number.
let port = 2770;

// 3. inside the server constructor, insert a method() => {}, in which we can use in order to describe the connect that was established. Identify the interaction between the client and the server and pass them down as the arguments of the method.

http.createServer((request,
    response) => {
    
// 4. after request and response is established, bind/assign the connection to the address, the listen() is used to bind and listen to a specific port whenever it's being accessed by the computer.

// write() - this allow us to insert messages or input to our page.
response.write(`Irasshai to port ${port}\n`)
response.write('hello za warudo\n')
response.write('LOL\n')
response.write('HAHAHA\n')
response.write('wkwkwkwkw')

// end() will allow us to identify a point where the transmission of data will end.
response.end()

}).listen(port);


console.log('server is running');

// when creating project, we should always think of more efficient ways in executing even the smallest task.


// TASK: Integrate the use of an NPM project in Node.js

    // 1. INITIALIZE an NPM into the local project.

        // 2 ways to perform this task:

        // 1. npm init

        // 2. npm init -y <= yes

        // package.JSON => this serves as the heart of any node projects as if as to record all the important metadata about the project (libraries, packages, function) that makes up the system or the enitre application

// now, that we have integrated an NPM into our project, lets solve some issues we are acing in our local project.

    // issue: needing to restart the project whenever changes occur.

    // solution: using NPM let's install a dependency that will allow to automatically fix (hotfix) changes in our app.

    // nodemon package

    // BASIC SKILLS using NPM
    
    // TO INSTALL
    // npm install <name of package>
    // npm i install <name of package>

    // TO INSTALL MULTIPLE PACKAGES
    // npm i nodemon express bcrypt

    // TO UNINSTALL
    // npm uninstall <name of package>
    // npm un <name of package>

// NODEMON - is a CLI utility tool, it's task is to wrap your NodeJS app and watch for any changes in the file system and automatically restart/hotfix the process

// NPM RUN - if you want to modify start/execute script except START
// EXAMPLE: npm run <x> => replace this instead of start in package.json
// start - global nodejs script
// dev - runs without plusgin and runs only the default runtime package

